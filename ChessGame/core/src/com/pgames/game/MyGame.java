package com.pgames.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.pgames.game.chess.scenes.GameState;
import com.pgames.game.chess.scenes.GameScene;
import com.pgames.game.chess.scenes.MenuScene;
import com.pgames.game.engine.scenes.SceneManager;

public class MyGame extends ApplicationAdapter {
	private GameState gameState;
	private SceneManager sceneManager;

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	@Override
	public void create () {
		gameState = GameState.Menu;
		sceneManager = new SceneManager(this);
		sceneManager.setActiveScene(gameState);
	}

	@Override
	public void render () {
		sceneManager.showActiveScene(gameState);
	}
	
	@Override
	public void dispose () {
		sceneManager.getActiveScene().dispose();
	}
}
