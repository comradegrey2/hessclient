package com.pgames.game.engine.system;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class ShapeRendererSystem {
    public enum ShapeType{
        Filled,
        Line
    }

    List<IEntity> entitiesFilled;
    List<IEntity> entitiesLine;

    public ShapeRendererSystem() {
        entitiesFilled = new ArrayList<IEntity>();
        entitiesLine = new ArrayList<IEntity>();
    }

    public void renderFilled(ShapeRenderer shapeRenderer){
        for (IEntity entity: entitiesFilled) {
            Color color = entity.getComponent(com.pgames.game.engine.entity.components.ShapeRenderer.class).getColor();
            Vector2 position = entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).getPosition();
            Vector2 size = entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).getSize();

            shapeRenderer.setColor(color.r, color.g, color.b, 1.0f);
            shapeRenderer.rect(position.x, position.y, size.x, size.y);
        }
    }

    public void renderLine(ShapeRenderer shapeRenderer){
        for (IEntity entity: entitiesLine) {
            if (entity.isEnabled()) {
                Color color = entity.getComponent(com.pgames.game.engine.entity.components.ShapeRenderer.class).getColor();
                Vector2 position = entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).getPosition();
                Vector2 size = entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).getSize();

                shapeRenderer.setColor(color.r, color.g, color.b, 1.0f);
                shapeRenderer.rect(position.x, position.y, size.x, size.y);
            }
        }
    }

    public void add(IEntity entity, ShapeType type){
        if (type == ShapeType.Filled) {
            entitiesFilled.add(entity);
        } else {
            entitiesLine.add(entity);
        }
    }
}
