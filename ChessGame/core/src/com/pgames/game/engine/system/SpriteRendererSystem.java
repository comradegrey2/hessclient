package com.pgames.game.engine.system;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.entity.components.SpriteRenderer;
import com.pgames.game.engine.entity.components.Text;
import com.pgames.game.engine.entity.components.Transform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class SpriteRendererSystem {
    public enum ShapeType{
        Sprite,
        Text
    }

    List<IEntity> entitiesSprites;
    List<IEntity> entitiesFonts;

    public SpriteRendererSystem() {
        entitiesSprites = new ArrayList<IEntity>();
        entitiesFonts = new ArrayList<IEntity>();
    }

    public void renderSprites(SpriteBatch batch){
        for (IEntity entity: entitiesSprites) {
            if (entity.isEnabled()) {
                Vector2 position = entity.getComponent(Transform.class).getPosition();
                Sprite sprite =  entity.getComponent(SpriteRenderer.class).getSprite();
                sprite.setPosition(position.x, position.y);
                sprite.draw(batch);
            }
        }
    }

    public void renderFont(SpriteBatch batch){
        for (IEntity entity: entitiesFonts) {
            if (entity.isEnabled()) {
                BitmapFont font = entity.getComponent(Text.class).getFont();
                String text = entity.getComponent(Text.class).getText();
                Vector2 position = entity.getComponent(Transform.class).getPosition();
                Vector2 offset = entity.getComponent(Text.class).getOffset();
                font.draw(batch, text, position.x + offset.x, position.y + offset.y);
            }
        }
    }

    public void add(IEntity entity, ShapeType type){
        if (type == ShapeType.Sprite) {
            entitiesSprites.add(entity);
        } else {
            entitiesFonts.add(entity);
        }
    }
}
