package com.pgames.game.engine.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public class ShapeRenderer implements Resource {
    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        entity.addComponent(new com.pgames.game.engine.entity.components.ShapeRenderer(entity));
        XmlReader.Element color = component.getChildByName("color");
        if (color != null) {
            float r = color.getFloat("r");
            float g = color.getFloat("g");
            float b = color.getFloat("b");
            float a = color.getFloat("a");
            entity.getComponent(com.pgames.game.engine.entity.components.ShapeRenderer.class).setColor(new Color(r, g, b,a));
        }

    }
}
