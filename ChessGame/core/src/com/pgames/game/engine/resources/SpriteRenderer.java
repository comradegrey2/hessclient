package com.pgames.game.engine.resources;

import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public class SpriteRenderer implements Resource {
    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        XmlReader.Element path = component.getChildByName("path");
        if (path != null) {
            String p = path.getText();
            entity.addComponent(new com.pgames.game.engine.entity.components.SpriteRenderer(p, entity));
        }
        XmlReader.Element region = component.getChildByName("region");
        if (region != null) {
            int x = region.getInt("x");
            int y = region.getInt("y");
            int width = region.getInt("width");
            int height = region.getInt("height");
            entity.getComponent(com.pgames.game.engine.entity.components.SpriteRenderer.class).setRegion(x,y,width,height);
        }
    }
}
