package com.pgames.game.engine.resources;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public class Transform implements Resource {

    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        entity.addComponent(new com.pgames.game.engine.entity.components.Transform(entity));
        XmlReader.Element position = component.getChildByName("position");
        if (position != null) {
            float x = position.getFloat("x");
            float y = position.getFloat("y");
            entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).setPosition(new Vector2(x, y));
        }
        XmlReader.Element size = component.getChildByName("size");
        if (size != null) {
            float x = size.getFloat("x");
            float y = size.getFloat("y");
            entity.getComponent(com.pgames.game.engine.entity.components.Transform.class).setSize(new Vector2(x, y));
        }
    }
}
