package com.pgames.game.engine.resources;

import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public interface Resource {
    public void create(XmlReader.Element component, IEntity entity);
}
