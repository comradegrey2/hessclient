package com.pgames.game.engine.resources;

import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public class Button implements Resource {
    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        entity.addComponent(new com.pgames.game.engine.entity.components.Button(entity));
    }
}
