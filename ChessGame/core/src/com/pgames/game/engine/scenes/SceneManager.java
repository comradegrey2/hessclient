package com.pgames.game.engine.scenes;

import com.pgames.game.MyGame;
import com.pgames.game.chess.scenes.GameScene;
import com.pgames.game.chess.scenes.GameState;
import com.pgames.game.chess.scenes.MenuScene;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public class SceneManager {
    public GameState activeState;
    IScene activeScene;
    Map<Integer,IScene> scenes;
    MyGame game;

    public SceneManager(MyGame game){
        scenes = new HashMap<Integer, IScene>();
        this.game = game;
    }

    public IScene getActiveScene() {
        return activeScene;
    }

    public void setActiveScene(GameState gameState) {
        switch (gameState){
            case Menu:
                activeScene = new MenuScene(game);
                activeState = GameState.Menu;
                break;
            case Game:
                activeScene = new GameScene(game);
                activeState = GameState.Game;
                break;
            default:
                break;
        }
    }

    public void showActiveScene(GameState gameState) {
        if (gameState != activeState) {
            activeState = gameState;
            setActiveScene(gameState);
        }
        activeScene.render();
    }
}
