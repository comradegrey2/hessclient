package com.pgames.game.engine.scenes;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public interface IScene {
    void render();
    void dispose();
}
