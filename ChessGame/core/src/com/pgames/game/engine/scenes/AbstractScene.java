package com.pgames.game.engine.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.pgames.game.MyGame;
import com.pgames.game.engine.system.ShapeRendererSystem;
import com.pgames.game.engine.system.SpriteRendererSystem;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public abstract class AbstractScene implements IScene{

    protected MyGame game;
    protected Color clearColor;
    SpriteBatch batch;
    ShapeRenderer shapeRenderer;

    protected ShapeRendererSystem shapeRendererSystem;
    protected SpriteRendererSystem spriteRendererSystem;


    public AbstractScene(MyGame game) {
        this.game = game;
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        clearColor = new Color(Color.CYAN);
        shapeRendererSystem = new ShapeRendererSystem();
        spriteRendererSystem = new SpriteRendererSystem();
        init();
    }

    protected abstract void init();
    protected abstract void update();

    protected void setClearColor(Color color) {
        clearColor = color;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRendererSystem.renderFilled(shapeRenderer);
        shapeRenderer.end();

        batch.begin();
        spriteRendererSystem.renderSprites(batch);
        spriteRendererSystem.renderFont(batch);
        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRendererSystem.renderLine(shapeRenderer);
        shapeRenderer.end();

        update();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
