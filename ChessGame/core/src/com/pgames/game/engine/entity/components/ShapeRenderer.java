package com.pgames.game.engine.entity.components;

import com.badlogic.gdx.graphics.Color;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class ShapeRenderer extends Component {
    private Color color;

    public ShapeRenderer(IEntity entity) {
        super(entity);
        color = Color.WHITE;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
