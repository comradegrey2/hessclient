package com.pgames.game.engine.entity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class Component {
    IEntity currentEntity;

    public Component(IEntity entity) {
        currentEntity = entity;
    }

    public IEntity getEntity() {
        return currentEntity;
    }

    public void setEntity(IEntity currentEntity) {
        this.currentEntity = currentEntity;
    }
}
