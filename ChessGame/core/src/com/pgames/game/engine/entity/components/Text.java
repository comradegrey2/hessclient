package com.pgames.game.engine.entity.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class Text extends Component {
    private String text;
    private BitmapFont bitmapFont;
    private Vector2 offset;

    public Text(IEntity entity) {
        super(entity);
        text = "text";
        bitmapFont = new BitmapFont();
        bitmapFont.setColor(Color.RED);
        offset = new Vector2(0, 0);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }



    public void setTextColor(Color textColor) {
        bitmapFont.setColor(textColor);
    }

    public BitmapFont getFont() {
        return bitmapFont;
    }

    public void setFont(BitmapFont bitmapFont) {
        this.bitmapFont = bitmapFont;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public void setOffset(Vector2 offset) {
        this.offset = offset;
    }
}
