package com.pgames.game.engine.entity.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class Button extends Component {
    Vector2 position;
    Vector2 size;

    public Button(IEntity entity) {
        super(entity);
        position = entity.getComponent(Transform.class).getPosition();
        size = entity.getComponent(Transform.class).getSize();
    }

    public boolean isClicked() {
        if (Gdx.input.justTouched() ) {
            float realY = 600 - Gdx.input.getY();
            boolean horizontal = Gdx.input.getX() > position.x && Gdx.input.getX() < position.x + size.x;
            boolean vertical = realY > position.y  && realY < position.y + size.y;
            return horizontal && vertical;
        }
        return false;

    }
}
