package com.pgames.game.engine.entity.components;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class Transform extends Component {
    private Vector2 position;
    private Vector2 size;


    public Transform(IEntity entity) {
        super(entity);
        position = new Vector2(0, 0);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size = size;
    }
}
