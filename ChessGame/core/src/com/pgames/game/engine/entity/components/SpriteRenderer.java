package com.pgames.game.engine.entity.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class SpriteRenderer extends Component{

    private Sprite sprite;
    private Texture texture;

    public SpriteRenderer(String path, IEntity entity) {
        super(entity);
        texture = new Texture(Gdx.files.internal(path));
        sprite = new Sprite(texture, texture.getWidth(), texture.getHeight());
        sprite.setPosition(0, 0);
    }

    public void setRegion(int x, int y, int width, int height) {
        sprite.setSize(width, height);
        sprite.setRegion(x, y, width, height);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setSpritePosition(Vector2 position){
        sprite.setPosition(position.x, position.y);
    }
}
