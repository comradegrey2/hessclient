package com.pgames.game.engine.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.chess.resources.*;
import com.pgames.game.engine.resources.*;

import java.io.IOException;
import java.util.*;

/**
 * Created by comradeGrey on 06.03.2017.
 */
public class Entity implements IEntity{
    protected boolean enabled;
    HashSet<Class> allComponentsClasses;
    Map<Component, List<Class<? extends Component>>> map;

    public Entity(String path, String name) {
        allComponentsClasses = new HashSet<Class>();
        map = new HashMap<Component, List<Class<? extends Component>>>();
        enabled = true;
        deserialize(path, name);
    }
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public <T extends Component> void addComponent(T component){
        List<Class<? extends Component>> list = new ArrayList<Class<? extends Component>>();
        if (component.getClass().getSuperclass() == Component.class){
            allComponentsClasses.add(component.getClass());
            map.put(component, list);
        } else {
            Class clazz = component.getClass();
            while (clazz != Component.class) {
                allComponentsClasses.add(clazz);
                clazz = clazz.getSuperclass();
                list.add(clazz);
            }
            map.put(component, list);
        }
    }

    @Override
    public <T extends Component> T getComponent(Class<T> clazz){
        if (!allComponentsClasses.contains(clazz)) return null;

        Component component = null;
        for (Map.Entry<Component, List<Class<? extends Component>>> entry : map.entrySet()) {
            component = entry.getKey();
            if (component.getClass() == clazz) {
                return (T)component;
            }
            for (Class c : entry.getValue()){
                if (c.equals(clazz)) {
                    return (T)component;
                }
            }
        }

        return (T)component;
    }

    private void deserialize(String path, String name) {
        try {
            XmlReader xmlReader = new XmlReader();
            FileHandle file = Gdx.files.internal(path);
            XmlReader.Element root = xmlReader.parse(file);
            Array<XmlReader.Element> entities = root.getChildrenByName("entity");
            for (XmlReader.Element entity : entities) {
                if (entity.getAttribute("name").equals(name)){
                    Array<XmlReader.Element> components = entity.getChildByName("components").getChildrenByName("component");
                    for (XmlReader.Element component : components) {
                        if (component.getAttribute("type").equals("Transform")){
                            Resource res = new Transform();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("ShapeRenderer")){
                            Resource res = new ShapeRenderer();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Button")){
                            Resource res = new Button();
                            res.create(component, this);
                        } else if (component.getAttribute("type").equals("Text")){
                            Resource res = new Text();
                            res.create(component, this);
                        } else if (component.getAttribute("type").equals("SpriteRenderer")) {
                            Resource res = new SpriteRenderer();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Board")) {
                            Resource res = new Board();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Rook")) {
                            Resource res = new Rook();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Bishop")) {
                            Resource res = new Bishop();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("King")) {
                            Resource res = new King();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Knight")) {
                            Resource res = new Knight();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Pawn")) {
                            Resource res = new Pawn();
                            res.create(component, this);
                        }else if (component.getAttribute("type").equals("Queen")) {
                            Resource res = new Queen();
                            res.create(component, this);
                        }

                    }
                    return;
                }
            }
            Gdx.app.error("Entity not Found: ", String.format("entity: %s, in file: %s", name, path));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
