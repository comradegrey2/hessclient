package com.pgames.game.engine.entity;

/**
 * Created by comradeGrey on 06.03.2017.
 */
public interface IEntity {
    boolean isEnabled();
    void setEnabled(boolean enabled);
    <T extends Component> void addComponent(T component);
    <T extends Component> T getComponent(Class<T> clazz);

}
