package com.pgames.game.chess.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.pgames.game.MyGame;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.entity.Entity;
import com.pgames.game.engine.entity.components.Button;
import com.pgames.game.engine.scenes.AbstractScene;
import com.pgames.game.engine.system.ShapeRendererSystem;
import com.pgames.game.engine.system.SpriteRendererSystem;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public class MenuScene extends AbstractScene {
    IEntity newGameBtn;
    IEntity exitBtn;

    public MenuScene(MyGame game) {
        super(game);
        setClearColor(Color.BLACK);
    }

    @Override
    protected void init() {
        newGameBtn = new Entity("data/scenes/menuScene.xml", "newGameBtn");
        shapeRendererSystem.add(newGameBtn, ShapeRendererSystem.ShapeType.Filled);
        spriteRendererSystem.add(newGameBtn, SpriteRendererSystem.ShapeType.Text);

        exitBtn =new Entity("data/scenes/menuScene.xml", "exitBtn");
        shapeRendererSystem.add(exitBtn, ShapeRendererSystem.ShapeType.Filled);
        spriteRendererSystem.add(exitBtn, SpriteRendererSystem.ShapeType.Text);
    }

    public void update() {
        if (newGameBtn.getComponent(Button.class).isClicked()){
            game.setGameState(GameState.Game);
        }

        if (exitBtn.getComponent(Button.class).isClicked()) {
            Gdx.app.exit();
        }
    }
}
