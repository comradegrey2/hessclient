package com.pgames.game.chess.scenes;

import com.badlogic.gdx.graphics.*;
import com.pgames.game.MyGame;
import com.pgames.game.chess.components.figures.Figure;
import com.pgames.game.chess.systems.BoardSystem;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.entity.Entity;
import com.pgames.game.engine.entity.components.Text;
import com.pgames.game.engine.scenes.AbstractScene;
import com.pgames.game.engine.system.ShapeRendererSystem;
import com.pgames.game.engine.system.SpriteRendererSystem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public class GameScene extends AbstractScene {

    IEntity menuBtn;
    IEntity board;
    IEntity figure;
    IEntity selectedGizmo;
    IEntity endGame;
    BoardSystem boardSystem;
    List<IEntity> figures;


    public GameScene(MyGame game) {
        super(game);
        setClearColor(Color.WHITE);
    }

    @Override
    protected void init() {
        menuBtn = new Entity("data/scenes/gameScene.xml", "menuBtn");
        shapeRendererSystem.add(menuBtn, ShapeRendererSystem.ShapeType.Filled);
        spriteRendererSystem.add(menuBtn, SpriteRendererSystem.ShapeType.Text);

        initNumbersText();
        initLettersText();

        board = new Entity("data/scenes/gameScene.xml", "board");
        spriteRendererSystem.add(board, SpriteRendererSystem.ShapeType.Sprite);


        figures = new ArrayList<IEntity>();

        /**************************************************************************/
        /***                         Black Figures                              ***/
        /**************************************************************************/
        figure = new Entity("data/scenes/gameScene.xml", "blackRook1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackRook2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackKnight1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackKnight2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackBishop1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackBishop2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackKing");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackQueen");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn3");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn4");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn5");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn6");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn7");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "blackPawn8");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);



        /**************************************************************************/
        /***                         White Figures                              ***/
        /**************************************************************************/
        figure = new Entity("data/scenes/gameScene.xml", "whiteRook1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteRook2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteKnight1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteKnight2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteBishop1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteBishop2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteKing");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whiteQueen");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn1");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn2");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn3");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn4");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn5");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn6");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn7");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);
        figure = new Entity("data/scenes/gameScene.xml", "whitePawn8");
        spriteRendererSystem.add(figure, SpriteRendererSystem.ShapeType.Sprite);
        figures.add(figure);



        selectedGizmo = new Entity("data/scenes/gameScene.xml", "selectedGizmo");
        shapeRendererSystem.add(selectedGizmo, ShapeRendererSystem.ShapeType.Line);
        selectedGizmo.setEnabled(false);

        endGame = new Entity("data/scenes/gameScene.xml", "endGame");
        spriteRendererSystem.add(endGame, SpriteRendererSystem.ShapeType.Text);
        endGame.setEnabled(false);


        boardSystem = new BoardSystem(board, figures, selectedGizmo);

    }

    public void update() {
        if (menuBtn.getComponent(com.pgames.game.engine.entity.components.Button.class).isClicked()) {
            game.setGameState(GameState.Menu);
        }
        if (boardSystem.getWinner() == null) {
            boardSystem.update();
        } else {
            String winSide = "";
            if (boardSystem.getWinner() == Figure.Side.Black) {
                winSide = "Black";
            } else {
                winSide = "White";
            }
            String winnerMsg = "Winner is " + winSide + "!";
            endGame.setEnabled(true);
            endGame.getComponent(Text.class).setText(winnerMsg);

        }
    }


    public void initLettersText() {
        String[] arr = {"A", "B", "C", "D", "E", "F", "G", "H"};
        for (int i = 0; i < 8; i++) {
            IEntity entity = new Entity("data/scenes/gameScene.xml", arr[i]);
            spriteRendererSystem.add(entity, SpriteRendererSystem.ShapeType.Text);
        }
    }

    public void initNumbersText() {
        String[] arr = {"1", "2", "3", "4", "5", "6", "7", "8"};
        for (int i = 0; i < 8; i++) {
            IEntity entity = new Entity("data/scenes/gameScene.xml", arr[i]);
            spriteRendererSystem.add(entity, SpriteRendererSystem.ShapeType.Text);
        }
    }
}
