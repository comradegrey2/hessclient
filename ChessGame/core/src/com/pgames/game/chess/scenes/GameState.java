package com.pgames.game.chess.scenes;

/**
 * Created by comradeGrey on 20.02.2017.
 */
public enum  GameState {
    Menu,
    Game
}
