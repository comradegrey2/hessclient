package com.pgames.game.chess.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.chess.components.figures.Figure;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.entity.components.Transform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 22.02.2017.
 */
public class BoardSystem {
    IEntity board;
    IEntity selectedFigure;
    List<IEntity> figures;
    IEntity selectedGizmo;
    Map<Vector2, IEntity> figuresOnBoard;
    Map<Vector2, Board.CellCode> boardMatrix;
    Figure.Side winner;
    Figure.Side currentMove;


    public BoardSystem(IEntity board, List<IEntity> figures, IEntity gizmo) {
        this.board = board;
        this.selectedGizmo = gizmo;
        this.figures = figures;
        figuresOnBoard = new HashMap<Vector2, IEntity>();
        boardMatrix = new HashMap<Vector2, Board.CellCode>();
        setStartPosition(figures);
        initFiguresOnBoard();
        currentMove = Figure.Side.White;
    }

    private void initFiguresOnBoard(){
        for (IEntity figure: figures) {
            Vector2 pos = figure.getComponent(Figure.class).getSpacePosition();
            figuresOnBoard.put(pos, figure);
        }
    }

    public Figure.Side getWinner() {
        return winner;
    }

    public void setWinner(Figure.Side winner) {
        this.winner = winner;
    }

    private void calculateBoardMatrix(Vector2 figureBoardPosition) {
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                //позиция выделенной фигуры
                if (figureBoardPosition.x == i && figureBoardPosition.y == j){
                    boardMatrix.put(new Vector2(i, j), Board.CellCode.CurrentFigure);
                } else {
                    Vector2 vec = new Vector2(i, j);
                    IEntity fig = figuresOnBoard.get(vec);
                    Board.CellCode code = null;
                    if (fig == null) {
                        code = Board.CellCode.Empty;
                        //если у выделенной фигуры цвет соотвествует проверямой
                    } else if(fig.getComponent(Figure.class).getSide().equals(selectedFigure.getComponent(Figure.class).getSide())) {
                        code = Board.CellCode.Friend;
                    } else {
                        code = Board.CellCode.Enemy;
                    }

                    boardMatrix.put(new Vector2(i, j), code);
                }
            }
        }

        //showMatrix(boardMatrix);
    }

    public void showMatrix( Map<Vector2, Board.CellCode> matrix) {
        for(Map.Entry<Vector2, Board.CellCode> entry: matrix.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        System.out.println("************");
    }

    private IEntity findFigure(Vector2 pos, Figure.Side currentMove) {
        IEntity entity = figuresOnBoard.get(pos);
        if (entity != null && entity.getComponent(Figure.class).getSide().equals(currentMove)) {
            return figuresOnBoard.get(pos);
        }
        return null;
    }

    public IEntity getBoard() {
        return board;
    }

    public void update() {
        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            unSelectFigure();
            selectedGizmo.setEnabled(false);
        } else if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
            if (selectedFigure != null) {
                selectedGizmo.setEnabled(true);
                calculateBoardMatrix(selectedFigure.getComponent(Figure.class).getSpacePosition());
                setNewPosition(selectedFigure.getComponent(Figure.class));
            } else {
                selectFigure();
            }

        }
    }

    private void setNewPosition(Figure figure) {
        Vector2 mousePosition = new Vector2(Gdx.input.getX(), Gdx.input.getY());
        Vector2 worldToSpace = getBoard().getComponent(Board.class).worldToSpace(mousePosition);
        Vector2 spaceToWorld = getBoard().getComponent(Board.class).spaceToWorld(worldToSpace);

        if (figure.isSelected() && figure.isCorrected(worldToSpace)) {
            if (figuresOnBoard.get(worldToSpace) != null) {
                if (figuresOnBoard.get(worldToSpace).getComponent(Figure.class).getSide().equals(selectedFigure.getComponent(Figure.class).getSide())){
                    return;
                }
            }
            List<Vector2> correctedCells = selectedFigure.getComponent(Figure.class).getCorrectedCells(worldToSpace, boardMatrix);

            boolean correct = false;

            for (Vector2 vec: correctedCells) {
                if (vec.equals(worldToSpace)) {
                    correct = true;
                    break;
                }
            }

            if (correct) {
                if (figuresOnBoard.get(worldToSpace) != null) {
                    kill(worldToSpace);
                }

                figuresOnBoard.put(selectedFigure.getComponent(Figure.class).getSpacePosition(), null);
                selectedFigure.getComponent(Transform.class).setPosition(spaceToWorld);
                selectedFigure.getComponent(Figure.class).setSpacePosition(worldToSpace);
                figuresOnBoard.put(worldToSpace, selectedFigure);
                selectedGizmo.setEnabled(false);
                changeCurrentMove();
                unSelectFigure();
            }
        }
    }

    private void changeCurrentMove() {
        if (currentMove == Figure.Side.White) {
            currentMove = Figure.Side.Black;
        } else {
            currentMove = Figure.Side.White;
        }
    }

    private void kill(Vector2 position){
        figures.remove(figuresOnBoard.get(position));
        if (figuresOnBoard.get(position).getComponent(Figure.class).toString().equals("King")) {
            Figure.Side side = figuresOnBoard.get(position).getComponent(Figure.class).getSide();
            if (side == Figure.Side.Black) {
                setWinner(Figure.Side.White);
            } else {
                setWinner(Figure.Side.Black);
            }
        }
        figuresOnBoard.get(position).setEnabled(false);
    }

    void setStartPosition(List<IEntity> figures) {
        float step = 56;
        Vector2 startWorldPos = new Vector2(53, 103);

        for (IEntity figure : figures ) {
            Vector2 spacePos = figure.getComponent(Figure.class).getSpacePosition();
            Vector2 stepVec = new Vector2((spacePos.x - 1) * step, (spacePos.y - 1) * step);
            figure.getComponent(Transform.class).setPosition(new Vector2(startWorldPos.x + stepVec.x, startWorldPos.y + stepVec.y));
        }
    }

     void unSelectFigure() {
        if (selectedFigure != null) {
            selectedFigure.getComponent(Figure.class).setSelected(false);
            selectedFigure = null;
        }
    }



    void  selectFigure() {
        Vector2 mousePosition = new Vector2(Gdx.input.getX(), Gdx.input.getY());
        Vector2 worldToSpace = getBoard().getComponent(Board.class).worldToSpace(mousePosition);
        Vector2 spaceToWorld = getBoard().getComponent(Board.class).spaceToWorld(worldToSpace);
        if (!spaceToWorld.equals(new Vector2(-1,-1))){
            selectedFigure = findFigure(worldToSpace, currentMove);
            if (selectedFigure != null) {
                selectedFigure.getComponent(Figure.class).setSelected(true);
                Vector2 offset = new Vector2(0, -2);
                selectedGizmo.getComponent(Transform.class).setPosition(new Vector2(spaceToWorld.x + offset.x, spaceToWorld.y + offset.y));
            }
        }
    }
}
