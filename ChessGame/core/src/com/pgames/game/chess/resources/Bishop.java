package com.pgames.game.chess.resources;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.chess.components.figures.Figure;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.resources.Resource;

/**
 * Created by comradeGrey on 08.03.2017.
 */
public class Bishop implements Resource {
    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        XmlReader.Element position = component.getChildByName("position");
        float x = 0, y = 0;
        Figure.Side s = null;
        if (position != null) {
            x = position.getFloat("x");
            y = position.getFloat("y");
        }
        XmlReader.Element side = component.getChildByName("side");
        if (side != null) {
            s = Figure.Side.values()[Integer.parseInt(side.getText())];
        }

        entity.addComponent(new com.pgames.game.chess.components.figures.Bishop(entity, new Vector2(x, y), s));
    }
}
