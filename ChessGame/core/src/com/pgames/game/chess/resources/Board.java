package com.pgames.game.chess.resources;

import com.badlogic.gdx.utils.XmlReader;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.resources.Resource;

/**
 * Created by comradeGrey on 07.03.2017.
 */
public class Board implements Resource {
    @Override
    public void create(XmlReader.Element component, IEntity entity) {
        entity.addComponent(new com.pgames.game.chess.components.Board(entity));
    }
}
