package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 25.02.2017.
 */
public class Knight extends Figure {

    public Knight(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);
    }

    @Override
    public boolean isCorrected(Vector2 target) {
        boolean x1 = ((target.x - 1) == spacePosition.x) || ((target.x + 1) == spacePosition.x);
        boolean y1 = (target.y - 2 == spacePosition.y) || (target.y + 2 == spacePosition.y);

        boolean x2 = ((target.x - 2) == spacePosition.x) || ((target.x + 2) == spacePosition.x);
        boolean y2 = (target.y - 1 == spacePosition.y) || (target.y + 1 == spacePosition.y);

        return (x1 && y1) || (x2 && y2);
    }

    @Override
    public void setValuesMovementCells() {
        movementMatrix.put(new Vector2(spacePosition.x + 1, spacePosition.y + 2), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x - 1, spacePosition.y + 2), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x + 1, spacePosition.y - 2), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x - 1, spacePosition.y - 2), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x + 2, spacePosition.y + 1), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x - 2, spacePosition.y - 1), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x + 2, spacePosition.y - 1), Board.CellCode.Way);
        movementMatrix.put(new Vector2(spacePosition.x - 2, spacePosition.y + 1), Board.CellCode.Way);
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target) {
        List<Vector2> list = new ArrayList<Vector2>();
        Vector2 pos = new Vector2(spacePosition.x + 1, spacePosition.y + 2);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x - 1, spacePosition.y + 2);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x + 1, spacePosition.y - 2);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x - 1, spacePosition.y - 2);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x +2, spacePosition.y + 1);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x - 2, spacePosition.y - 1);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x + 2, spacePosition.y - 1);
        addToList(list, pos);

        pos = new Vector2(spacePosition.x - 2, spacePosition.y + 1);
        addToList(list, pos);

        return list;
    }

    private void addToList(List<Vector2> list, Vector2 value){
        if (finalMatrix.get(value) != null) {
            int res = finalMatrix.get(value);
            if (res != 3) {
                list.add(value);
            }
        }
    }

}
