package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.*;

/**
 * Created by comradeGrey on 25.02.2017.
 */
public class Rook extends Figure {


    public Rook(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);

    }

    @Override
    public boolean isCorrected(Vector2 target) {
        boolean x = (target.x - spacePosition.x == 0) || (spacePosition.x - target.x  == 0);
        boolean y = (target.y - spacePosition.y == 0) || (spacePosition.y - target.y  == 0);
        return x || y;
    }

    @Override
    protected void setValuesMovementCells() {
        for (int i = 1; i < 9; i++) {
            Vector2 cell = new Vector2(i, spacePosition.y);
            if (!cell.equals(spacePosition)) {
                movementMatrix.put(cell, Board.CellCode.Way);
            }
        }

        for (int i = 1; i < 9; i++) {
            Vector2 cell = new Vector2(spacePosition.x, i);
            if (!cell.equals(spacePosition)) {
                movementMatrix.put(cell, Board.CellCode.Way);
            }
        }
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target){

        List<Vector2> list = new ArrayList<Vector2>();
        Vector2 vec = new Vector2(target.x - getSpacePosition().x, target.y - getSpacePosition().y);

        if (vec.x == 0.0 && vec.y > 0) {
            for (float i = getSpacePosition().y + 1; i < 9; i++) {
                int res = finalMatrix.get(new Vector2(getSpacePosition().x, i));
                if (res == 5) {
                    list.add(new Vector2(getSpacePosition().x, i));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(getSpacePosition().x, i));
                    }
                    break;
                }
            }
        } else if (vec.x == 0.0 && vec.y < 0) {
            for (float i = getSpacePosition().y - 1; i > 0; i--) {
                int res = finalMatrix.get(new Vector2(getSpacePosition().x, i));
                if (res == 5) {
                    list.add(new Vector2(getSpacePosition().x, i));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(getSpacePosition().x, i));
                    }
                    break;
                }
            }
        } else if (vec.x > 0.0 && vec.y == 0) {
            for (float i = getSpacePosition().x + 1; i < 9; i++) {
                int res = finalMatrix.get(new Vector2(i, getSpacePosition().y));
                if (res == 5) {
                    list.add(new Vector2(i, getSpacePosition().y));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(i, getSpacePosition().y));
                    }
                    break;
                }
            }
        } else if (vec.x < 0.0 && vec.y == 0) {
            for (float i = getSpacePosition().x - 1; i > 0; i--) {
                int res = finalMatrix.get(new Vector2(i, getSpacePosition().y));
                if (res == 5) {
                    list.add(new Vector2(i, getSpacePosition().y));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(i, getSpacePosition().y));
                    }
                    break;
                }
            }
        }
        return list;
    }
}
