package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 24.02.2017.
 */
public class Pawn extends Figure {
    public Pawn(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);
    }

    @Override
    public boolean isCorrected(Vector2 target) {
        return true;
    }

    @Override
    public void setValuesMovementCells() {
        Vector2 move = null;
        if (getSide() == Side.Black) {
            //если пешка не ходила, она может походить вперед на 2 клетки
            if (spacePosition.y == 7) {
                move = new Vector2(spacePosition.x, spacePosition.y - 2);
                movementMatrix.put(move, Board.CellCode.Way);
            }
            move = new Vector2(spacePosition.x, spacePosition.y - 1);
            movementMatrix.put(move, Board.CellCode.Way);

            move = new Vector2(spacePosition.x + 1, spacePosition.y - 1);
            movementMatrix.put(move, Board.CellCode.Way);

            move = new Vector2(spacePosition.x - 1, spacePosition.y - 1);
            movementMatrix.put(move, Board.CellCode.Way);


        } else {
            if (spacePosition.y == 2) {
                move = new Vector2(spacePosition.x, spacePosition.y + 2);
                movementMatrix.put(move, Board.CellCode.Way);
            }
            move = new Vector2(spacePosition.x, spacePosition.y + 1);
            movementMatrix.put(move, Board.CellCode.Way);

            move = new Vector2(spacePosition.x + 1, spacePosition.y + 1);
            movementMatrix.put(move, Board.CellCode.Way);

            move = new Vector2(spacePosition.x - 1, spacePosition.y + 1);
            movementMatrix.put(move, Board.CellCode.Way);
        }
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target) {
        Vector2 move = null;
        List<Vector2> list = new ArrayList<Vector2>();
        if (getSide() == Side.Black) {
            if (spacePosition.y == 7) {
                move = new Vector2(spacePosition.x, spacePosition.y - 2);
                addToList(list, move, 5);
            }

            move = new Vector2(spacePosition.x, spacePosition.y - 1);
            addToList(list, move, 5);

            move = new Vector2(spacePosition.x + 1, spacePosition.y - 1);
            addToList(list, move, 9);

            move = new Vector2(spacePosition.x - 1, spacePosition.y - 1);
            addToList(list, move, 9);
        } else {
            if (spacePosition.y == 2) {
                move = new Vector2(spacePosition.x, spacePosition.y + 2);
                addToList(list, move, 5);
            }

            move = new Vector2(spacePosition.x, spacePosition.y + 1);
            addToList(list, move, 5);

            move = new Vector2(spacePosition.x + 1, spacePosition.y + 1);
            addToList(list, move, 9);

            move = new Vector2(spacePosition.x - 1, spacePosition.y + 1);
            addToList(list, move, 9);
        }
        return list;
    }

    private void addToList(List<Vector2> list, Vector2 value, int reqVal){
        if (finalMatrix.get(value) != null) {
            int res = finalMatrix.get(value);
            if (res == reqVal) {
                list.add(value);
            }
        }
    }
}
