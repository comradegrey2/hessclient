package com.pgames.game.chess.components;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;
import com.pgames.game.engine.entity.components.Transform;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by comradeGrey on 21.02.2017.
 */
public class Board extends Component {
    public enum CellCode {
        Empty,
        CurrentFigure,
        Way,
        Friend,
        Enemy
    }

    Vector2 coodinates;
    private float cellSize = 56.0f;

    public Board(IEntity entity) {
        super(entity);
    }

    public Vector2 getCoodinates() {
        return coodinates;
    }

    /**
     * перевод мировых координат в координаты доски
     * @param pos
     * @return
     */
    public Vector2 worldToSpace(Vector2 pos) {
        float realY = 600 - pos.y;
        Vector2 localPosition = new Vector2(-1.0f, -1.0f);
        Vector2 boardPos = getEntity().getComponent(Transform.class).getPosition();
        Vector2 boardSize = getEntity().getComponent(Transform.class).getSize();
        boolean correctX = (pos.x - boardPos.x > 0.0f) && (pos.x - boardPos.x) < boardSize.x;
        boolean correctY = (realY - boardPos.y > 0.0f) && (realY - boardPos.y) < boardSize.y;
        if (correctX && correctY) {
            localPosition = new Vector2(pos.x - boardPos.x, realY - boardPos.y);
            localPosition = new Vector2(MathUtils.ceil(localPosition.x / cellSize), MathUtils.ceil(localPosition.y / cellSize));
        }

        return localPosition;
    }

    /**
     * перевод координат доски в мировые
     * @param position
     * @return
     */
    public Vector2 spaceToWorld(Vector2 position) {
        if (position.equals(new Vector2(-1,-1))) return position;
        Vector2 boardPos = getEntity().getComponent(Transform.class).getPosition();
        Vector2 newPosition = new Vector2(cellSize * (position.x - 1) , cellSize * (position.y- 1) );
        return new Vector2(newPosition.x + boardPos.x + 3.0f, newPosition.y + boardPos.y + 3.0f);
    }

    public static int convertCode(CellCode code) {
        int result = -1;
        switch (code) {
            case Empty:
                result = 0;
                break;
            case CurrentFigure:
                result = 1;
                break;
            case Way:
                result = 5;
                break;
            case Friend:
                result = 3;
                break;
            case Enemy:
                result = 4;
                break;
        }
        return result;
    }
}
