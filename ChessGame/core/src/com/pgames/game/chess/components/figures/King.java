package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 25.02.2017.
 */
public class King extends Figure {
    public King(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);
    }

    @Override
    public boolean isCorrected(Vector2 target) {
        boolean x = (target.x - 1 == spacePosition.x) || (target.x + 1 == spacePosition.x) || (target.x == spacePosition.x);
        boolean y = (target.y - 1 == spacePosition.y) || (target.y + 1 == spacePosition.y) || (target.y == spacePosition.y);
        return x && y;
    }

    @Override
    public void setValuesMovementCells() {
        Vector2 move = new Vector2(spacePosition.x - 1, spacePosition.y);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x - 1, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x - 1, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target) {
        List<Vector2> list = new ArrayList<Vector2>();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                Vector2 move = new Vector2(i + spacePosition.x, j + spacePosition.y);
                if (!move.equals(spacePosition)) {
                    if (finalMatrix.get(move) == null) continue;
                    int res = finalMatrix.get(move);
                    if (res == 5 || res == 9) {
                        list.add(move);
                    }
                }
            }
        }

        return list;
    }

    @Override
    public String toString() {
        return "King";
    }
}
