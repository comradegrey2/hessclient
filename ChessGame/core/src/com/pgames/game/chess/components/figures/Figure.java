package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.Component;
import com.pgames.game.engine.entity.IEntity;

import java.util.*;

/**
 * Created by comradeGrey on 21.02.2017.
 */
public abstract class Figure extends Component{
    public enum Side {
        White,
        Black
    }

    protected boolean isSelected;
    protected Vector2 spacePosition;
    protected Map<Vector2, Board.CellCode> movementMatrix;
    Map<Vector2, Integer> finalMatrix;
    protected Side side;

    public Figure(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity);
        this.spacePosition = spacePosition;
        isSelected = false;
        movementMatrix = new HashMap<Vector2, Board.CellCode>();
        this.side = side;
        calculateMovement();
    }

    public Side getSide() {
        return side;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public abstract boolean isCorrected(Vector2 target);

    protected abstract void setValuesMovementCells();

    private void calculateMovement() {
        movementMatrix.clear();

        //сначала матрица заполняется кодами пустых ячеек
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                movementMatrix.put(new Vector2(i, j), Board.CellCode.Empty);
            }
        }

        //затем кодом позиции фигуры
        movementMatrix.put(new Vector2(spacePosition.x, spacePosition.y),  Board.CellCode.CurrentFigure);

        setValuesMovementCells();
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Vector2 getSpacePosition() {
        return spacePosition;
    }

    public void setSpacePosition(Vector2 spacePosition) {
        this.spacePosition = spacePosition;
    }

    protected abstract List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target);

    public List<Vector2> getCorrectedCells(Vector2 target, Map<Vector2, Board.CellCode> boardMatrix) {
        calculateMovement();
        finalMatrix = new HashMap<Vector2, Integer>();
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                int val1 = Board.convertCode(boardMatrix.get(new Vector2(i, j)));
                int val2 = Board.convertCode(movementMatrix.get(new Vector2(i, j)));
                int result = val1 + val2;
                if (result != Board.convertCode(Board.CellCode.Empty)) {
                    finalMatrix.put(new Vector2(i, j), val1 + val2);
                }
            }
        }

        return correctCells(finalMatrix, target);
    }

    protected void showMatrix() {
        for (Map.Entry<Vector2, Integer> matrix: finalMatrix.entrySet()){
            System.out.println(matrix.getKey() + ":" + matrix.getValue());
        }
        System.out.println("***************");
    }

}
