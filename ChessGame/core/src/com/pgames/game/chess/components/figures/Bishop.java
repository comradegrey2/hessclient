package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 25.02.2017.
 */
public class Bishop extends Figure {
    public Bishop(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);
    }

    @Override
    public boolean isCorrected(Vector2 target) {
        float dif = Math.abs(target.x - spacePosition.x);
        if (target.y - spacePosition.y == dif) {
            return true;
        }
        dif = Math.abs(spacePosition.x - target.x);
        if (spacePosition.y - target.y  == dif) {
            return true;
        }
        return false;
    }

    @Override
    public void setValuesMovementCells() {
        Vector2 pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x - 1, pos.y - 1);
            if (pos.x < 1 || pos.y < 1) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x + 1, pos.y + 1);
            if (pos.x > 8 || pos.y > 8) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x - 1, pos.y + 1);
            if (pos.x < 1 || pos.y < 1) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x + 1, pos.y - 1);
            if (pos.x > 8 || pos.y > 8) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target) {
        List<Vector2> list = new ArrayList<Vector2>();
        calcBishopMove(list, target);
        return list;
    }

    private void calcBishopMove(List<Vector2> list, Vector2 target) {
        Vector2 vec = new Vector2(target.x - getSpacePosition().x, target.y - getSpacePosition().y);
        Vector2 pos = spacePosition;
        int res;
        if (vec.x < 0 && vec.y < 0) {
            while (true) {
                pos = new Vector2(pos.x - 1, pos.y - 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if(res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x > 0 && vec.y > 0) {
            while (true) {
                pos = new Vector2(pos.x + 1, pos.y + 1);
                if (pos.x > 8 || pos.y > 8) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x > 0 && vec.y < 0) {
            while (true) {
                pos = new Vector2(pos.x + 1, pos.y - 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x < 0 && vec.y > 0) {
            while (true) {
                pos = new Vector2(pos.x - 1, pos.y + 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        }
    }
}
