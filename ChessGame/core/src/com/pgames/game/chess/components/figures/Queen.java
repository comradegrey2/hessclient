package com.pgames.game.chess.components.figures;

import com.badlogic.gdx.math.Vector2;
import com.pgames.game.chess.components.Board;
import com.pgames.game.engine.entity.IEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by comradeGrey on 25.02.2017.
 */
public class Queen extends Figure {
    public Queen(IEntity entity, Vector2 spacePosition, Side side) {
        super(entity, spacePosition, side);
    }

    @Override
    public boolean isCorrected(Vector2 target) {
        //ход короля
        boolean x = (target.x - 1 == spacePosition.x) || (target.x + 1 == spacePosition.x) || (target.x == spacePosition.x);
        boolean y = (target.y - 1 == spacePosition.y) || (target.y + 1 == spacePosition.y) || (target.y == spacePosition.y);
        if (x && y) return true;

        //ход слона
        float dif = Math.abs(target.x - spacePosition.x);
        if (target.y - spacePosition.y == dif) {
            return true;
        }
        dif = Math.abs(spacePosition.x - target.x);
        if (spacePosition.y - target.y  == dif) {
            return true;
        }

        //ход ладьи
        x = (target.x - spacePosition.x == 0) || (spacePosition.x - target.x  == 0);
        y = (target.y - spacePosition.y == 0) || (spacePosition.y - target.y  == 0);
        if (x || y) return true;

        return false;
    }

    @Override
    public void setValuesMovementCells() {
        initKingMove();
        initBishopMove();
        initRookMove();
    }

    @Override
    protected List<Vector2> correctCells(Map<Vector2, Integer> finalMatrix, Vector2 target) {
        List<Vector2> list = new ArrayList<Vector2>();
        calcKingMove(list);
        calcBishopMove(list, target);
        calcRookMove(list, target);

        return list;
    }

    private void initKingMove() {
        Vector2 move = new Vector2(spacePosition.x - 1, spacePosition.y);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x - 1, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x + 1, spacePosition.y - 1);
        movementMatrix.put(move, Board.CellCode.Way);

        move = new Vector2(spacePosition.x - 1, spacePosition.y + 1);
        movementMatrix.put(move, Board.CellCode.Way);
    }

    private void initBishopMove() {
        Vector2 pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x - 1, pos.y - 1);
            if (pos.x < 1 || pos.y < 1) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x + 1, pos.y + 1);
            if (pos.x > 8 || pos.y > 8) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x - 1, pos.y + 1);
            if (pos.x < 1 || pos.y < 1) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

        pos = spacePosition;
        while (true) {
            pos = new Vector2(pos.x + 1, pos.y - 1);
            if (pos.x > 8 || pos.y > 8) {
                break;
            }
            movementMatrix.put(pos, Board.CellCode.Way);
        }

    }

    private void initRookMove() {
        for (int i = 1; i < 9; i++) {
            Vector2 cell = new Vector2(i, spacePosition.y);
            if (!cell.equals(spacePosition)) {
                movementMatrix.put(cell, Board.CellCode.Way);
            }
        }

        for (int i = 1; i < 9; i++) {
            Vector2 cell = new Vector2(spacePosition.x, i);
            if (!cell.equals(spacePosition)) {
                movementMatrix.put(cell, Board.CellCode.Way);
            }
        }
    }

    private void calcKingMove(List<Vector2> list) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                Vector2 move = new Vector2(i + spacePosition.x, j + spacePosition.y);
                if (!move.equals(spacePosition)) {
                    if (finalMatrix.get(move) == null) continue;
                    int res = finalMatrix.get(move);
                    if (res == 5 || res == 9) {
                        list.add(move);
                    }
                }
            }
        }
    }

    private void calcBishopMove(List<Vector2> list, Vector2 target) {
        Vector2 vec = new Vector2(target.x - getSpacePosition().x, target.y - getSpacePosition().y);
        Vector2 pos = spacePosition;
        int res;
        if (vec.x < 0 && vec.y < 0) {
            while (true) {
                pos = new Vector2(pos.x - 1, pos.y - 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if(res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x > 0 && vec.y > 0) {
            while (true) {
                pos = new Vector2(pos.x + 1, pos.y + 1);
                if (pos.x > 8 || pos.y > 8) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x > 0 && vec.y < 0) {
            while (true) {
                pos = new Vector2(pos.x + 1, pos.y - 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        } else if (vec.x < 0 && vec.y > 0) {
            while (true) {
                pos = new Vector2(pos.x - 1, pos.y + 1);
                if (pos.x < 1 || pos.y < 1) break;
                if (finalMatrix.get(pos) != null) {
                    res = finalMatrix.get(pos);
                    if (res == 5) {
                        list.add(pos);
                    } else if (res == 9) {
                        list.add(pos);
                        break;
                    }
                }
            }
        }
    }

    private void calcRookMove(List<Vector2> list, Vector2 target) {
        Vector2 vec = new Vector2(target.x - getSpacePosition().x, target.y - getSpacePosition().y);

        if (vec.x == 0.0 && vec.y > 0) {
            for (float i = getSpacePosition().y + 1; i < 9; i++) {
                int res = finalMatrix.get(new Vector2(getSpacePosition().x, i));
                if (res == 5) {
                    list.add(new Vector2(getSpacePosition().x, i));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(getSpacePosition().x, i));
                    }
                    break;
                }
            }
        } else if (vec.x == 0.0 && vec.y < 0) {
            for (float i = getSpacePosition().y - 1; i > 0; i--) {
                int res = finalMatrix.get(new Vector2(getSpacePosition().x, i));
                if (res == 5) {
                    list.add(new Vector2(getSpacePosition().x, i));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(getSpacePosition().x, i));
                    }
                    break;
                }
            }
        } else if (vec.x > 0.0 && vec.y == 0) {
            for (float i = getSpacePosition().x + 1; i < 9; i++) {
                int res = finalMatrix.get(new Vector2(i, getSpacePosition().y));
                if (res == 5) {
                    list.add(new Vector2(i, getSpacePosition().y));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(i, getSpacePosition().y));
                    }
                    break;
                }
            }
        } else if (vec.x < 0.0 && vec.y == 0) {
            for (float i = getSpacePosition().x - 1; i > 0; i--) {
                int res = finalMatrix.get(new Vector2(i, getSpacePosition().y));
                if (res == 5) {
                    list.add(new Vector2(i, getSpacePosition().y));
                } else {
                    if (res == 9) {
                        list.add(new Vector2(i, getSpacePosition().y));
                    }
                    break;
                }
            }
        }
    }

}
